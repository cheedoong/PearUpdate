# PearUpdate
Automatic Incremental Software Updater for Pear & AnanFlow

## GenPatches
Server Side. 
Compare the difference of all files in old and new directories, generate the patch files and the description file. 

## SewPatches
Client Side. 
Check periodically. If there is a new version, read the description file and then update each files. 
