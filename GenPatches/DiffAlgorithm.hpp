#ifndef DiffAlgorithm_HPP
#define DiffAlgorithm_HPP

#include <string>

using namespace std;

class DiffAlgorithm
{
public:
    virtual int Do(const string &old_file, const string &new_file, const string &patch_file) = 0;
};

#endif // DiffAlgorithm_HPP
