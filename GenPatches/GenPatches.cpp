#include <iostream>
#include "GenPatch.hpp"
#include "GenPatches.hpp"
/*
Ref:
http://stackoverflow.com/questions/9162969/how-can-a-c-binary-replace-itself
http://stackoverflow.com/questions/1712033/replacing-a-running-executable-in-linux
http://stackoverflow.com/questions/3363592/update-program-by-writing-to-the-currently-executing-file
On Windows it is not possible to remove the executable of a running program, but possible to rename it:
download app.exe~
rename running app.exe to app.exe.old
rename app.exe~ to app.exe
when restarting remove app.exe.old
*/

using namespace std;

int main()
{
    /*
    GenPatch<DiffAlgorithmBSDiff> Gen_Patch;
    string oldfile = "./testdata/chrome64_1.exe";
    string newfile = "./testdata/chrome64_2.exe";
    string patchfile = "./testdata/chrome64_1_2.patch";
    Gen_Patch.Do(oldfile, newfile, patchfile);
    */
    GenPatches GenPathes("./test_old", "./test_new", "./test_patch", "./test_desc");
    GenPathes.Do();
    cout << "Hello world!" << endl;
    return 0;
}
