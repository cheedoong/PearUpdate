#ifndef GenPatch_HPP
#define GenPatch_HPP

#include "DiffAlgorithm.hpp"
#include "DiffAlgorithmBSDiff.hpp"

template <class DA>
class GenPatch
{
private:
    DA m_da;
public:
    GenPatch() { }
    ~GenPatch() { }
    int Do(const string &old_file, const string &new_file, const string &patch_file)
    {
        return m_da.Do(old_file, new_file, patch_file);
    }
};


#endif // GenPatch_HPP
